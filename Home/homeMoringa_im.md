---
title: Mission Statement
date: 2018-08-11
---

# Designing Lifestyles for Change

## *Bespoke Lifestyle* ~ Luxuriate In Nature
### **Align with the power that manifests everything in the universe, NATURE.**

***mission - inspire, nourish and support environmental creativity***

***Toward Developing The Potential Given to us by Nature***

A new ***Eco Conscious*** breed of ***Luxury Lifestyle Designers*** has evolved 
from the adage of "The American Dream", the wild places of this world need us 
to do better than that.

If there is no personal transformation there is no generational transformation. It's time to step out from the story of our past and write a New Future, a new story.

We are passionate about protecting the natural world in which we all live. 
Discovering and sharing solutions to everyday environmental impacts that we 
inflict on our surroundings. Creating a healthy living environment, turning 
the tables from an environmental withdraw to a environmental deposit for the 
future. 

We are all part of the great chain of life, and poisons in the environment will 
find their way into our ecosystems and eventually our own bodies. 

Our mission is to encourage each of us to figure out a way to make a statement. 
To do something — or not do something — to enrich our habitat and environment. People with 
passion, purpose and practical skills are needed to make the world a better place.

**Lifestyle; the Legacy your Karma left Behind.**

**We shall come to honor all of life sooner or later. Our choices are; when that shall happen,
and the quality of experience that we shall have as we learn.**

# *Private Freelance Consultant ~ Anna*

**I actively invest and share my background, unique experiences, network and resources into what I do and Whom I work with.** 

**Helping individuals get clear on what it is they want to achieve and sustainably move toward their goals by cultivating symbiotic relationships between eachother and nature.**

**"Waste Management"- minimize that! Time, Resources, Energy, ..., Yours And The Environment. Minimize Your Waste!**

***Co-create Sustainable/Symbiotic Living Systems Towards Developing The Potential Given To Us By Nature."***

# The Vivarium
## ~ ***Place of Life*** ~

**A Resourse For Urban Sustainability ~ George Town, Penang**

### ***Offering***

# *Eco-Immersive Bespoke Lifestyle Experiences*
**Private Inhouse and Distance Counsel**

### ***Options Include***

~ **Stay and Experience The Vivarium** - select from a menu of in house ***"Bespoke Lifestyle Experiences"***

~ **The Vivarium Co-Host Experience** - Eco-Immersive Vivarium Co-Host Experience _ Live in The Vivarium and learn how to keep it alive, accept guests and share the knowledge gained. 

~ **Vivarium Co-Host and Cocurate** - Offer your own style trom The Vivarium with support.

# *Stay and Experience Renewal*

***Make Your Next Eco-Energy Exchange At The Vivarium.***