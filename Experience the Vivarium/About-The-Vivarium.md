---
title: The Vivarium Story 
date: 2018-08-11
---

# The Vivarium ~ place of Life
***Luxuriate In Nature***

## ~ *Bespoke Lifestyle* ~ 
### *Align with the power that manifests everything in the universe, **Nature.***
**Bring back to life the Symbiotic Relationship we have with Nature.**

**React To The **Environmental Realities** We Face.**

**Archaic Revival ~ a search for Cultural Models That Worked in the past.**

**Knowledge to design a more desirable, fulfilling, life supporting; Ecologically Modern and Realistic Lifestyle.** 

***The Vivarium ~ Curator Anna***_(myself): A curious desire to experience pure potentiality seeking expression from the un-manifest to the manifest.

## *Retreat to Home, Reconect to Source*

**Nostalgia meets Novelty**

_ a deep love, 

_ a childhood memory recreated.

**Step out into Nature**

How deep in the forest have you been or dream of being?

Where?

Do you remember stepping out into Nature?

From a cabin deep in the Arctic tundra,

from a forest lake side tent or shelter, 

a farmhouse in the countryside, 

... *Or*

**From ones third floor "Patio Apartment", discover a forest garden of edible, nutrient dense, medicinal plants.**

A space that pushes the edges of how to:

***Live A Fruitful & Luxurious Lifestyle.***

Cultivating strength, vitality and joy with intention and desire to generate actions that are evolutionary for yourself and those around you. 

**If there is no Personal Transformation, there is no Generational Transformation.**

This is a kind of road map, perspective, a collection of tools, that gives a glimpse of a future possibility.

# *Align with the power that manifests everything in the universe, **Nature.***

**It's Time for each of us to start comparing consumption and lifestyles, and checking this against nature's ability to provide for this consumption.** 

It's Time to own up to the realities of what that translates to for the generations to come. It's time to be Ecologically Realistic and get back to the heart of what Life is all about.

Having accepted this circumstance, responsibility then means the *ability* to have a creative response to the situation as it is Now. The *seeds of opportunity*, and awareness to take the moment and transform it into a better situation. 

***An opportunity for the creation of something new and beautiful.***

> Those who do not want to imitate anything, produce nothing. ~ S. Dali 

# Welcome to *The Vivarium* - where Nature is Art

**Cocurating Lifestyles that foster authentic empowerment and inspire future generations to seek the truth for themselves.**

**Mission**: To inspire, nourish and support environmental creativity.

**Objective**: *Develop The Potential Given To Us By Nature*.

**The Vivarium ~ place of life** - An interactive space for people to explore, learn, reflect, ... on how we interact, impact and live in our personal environments; homes, work, travel, ..., spaces. 

# Empowered Living ~ Intentionally Designed To Foster Growth

**Live in a space of inspiration and you will be inspired.**

Learn to pay it forward, make a vow, connect with our earth, and give back in a meaningful way. It all starts with developing your lifestyle to be in harmony with the natural environment, leave a growing legacy for the next generations.

# *Harmonize and Awaken the Natural Healing Energies.*

**Discover your natural physique.**

Looking after personal well-being, self care that goes far beyond the self. 

Connect to Nature and experience a more *Enlightened Lifestyle* designed to Nourish, Fortify and Heal the Body your Habitat and the Environment. 

Transform your wellbeing, develop the potential given to you by nature and experience the bodies natural ability to heal.

**When you heal the soil, you heal the soul.**

# Get your hands dirty

**We encourage everyone to experience themselves as part of the creative process.** 

With engaging, participatory lifestyle experiences ever gaining in popularity, we include our guests in All the systems, conservation and soil rehabilitation activities that take place at the Vivarium. There is no "behind the scene", everything is visable and easily discerned. All your questions will be answered and knowledge shared.  

# Experience Life, not escape but to enrich

As the Eco Conscious - Seekers of Authentic Experiences continue to sway towards the path less traveled, we offer a Open Alternative and Love Collaboration.

# Build Your Own Adventure

**Customize every element to your satisfaction.**

**Offering modular, immersive and authentic experiences** that cater directly to this need. Choose from our menu of programs or contact us directly for one on one cocreating.

**We shall come to honor all of life sooner or later. Our choices are; when that shall happen, and the quality of experience that we shall have as we learn.**

# *React To The Environmental Realities We Face.*

