---
title: The Vivarium Room Options ~ George Town, Penang
date: 2018-08-11
---

# Location

**George Town, Penang**

Due to the intermingling of the various ethnicities and religions that arrived on its shores, George Town acquired a large eclectic assortment of Colonial and Asian architectural styles. It also gained a reputation as gastronomic paradise. 

The Vivarium is central to many George Town attractions and just 500 meters from "Link Bike", a public bike rental station. Pulau Tikus Market/Hawker Center is 1.2km, Botanical Garden 3.2km and UNESCO World Heritage Site 2.6km. 

**The Neighborhood ~ Dunn Road**

The surrounding low density  neighborhood is tranquil with many large trees, and properties with plenty of green space. Dunn Road is one of the preferred addresses on the island today.

**The Building and Property**

**Dunn House** is a freehold, 3-story residential apartment building with just 15 exclusive units, each 1230 square feet in size. Dunn House is one of the pioneering boutique developments of George Town, Penang. In October 2015 We gained permission from the building management and residents to start a garden on the property. 

Enjoy a walk or have a picnic in the shared organic garden some of the residents tend on the property. 

***A Journey Of Soil Rehabilitation.***

*A Revival Of The Gaian Connection; When You Heal The Soil, You Heal The Soul.*

**The Vivarium ~ Place of Life** 

From our third floor, north-facing unit we enjoy a fantastic cross breeze with many large opening windows, planters and a modest patio. Additionally we run an evaporative cooler in the common area that keeps the entire space a pleasant micro climate. We consciously chose to be AC Free!

Our Private Medicinal Food Garden; boasts over 27 large 5 gallon (or larger) containers located outside the windows, as well as 22+ clay pots on the balcony. All plants are edible, medicinal or functional. Besides an assortment of herbs, aloe plants, and experimental seedlings like Durian! Our garden in home to 23 moringa oleifera trees, Papaya trees, coconut babies and Sacha Inchi vines. We also have a beautiful passion fruit vine that tries to take over the laundry wire. It's difficult to catalog the ever changing garden. Growing a diverse garden is part of what makes it a pleasure and easy to enjoy a eco-responsible lifestyle.

All rooms have several opening windows with a garden growing from a large concrete window box. Rooms have low voltage LED lighting providing a pleasant healthy light, a large ceiling fan, and mosquito net to cover the bed.

Peaceful nights sleep, a little sanctuary in the city. 
Spacious and private.

# Private Room Options

[**Queen Bed w/ Private Bath**](https://www.airbnb.com.sg/rooms/15590733?s=51)

This room features; a dedicated evaporative cooler, for the addition of essential oils as part of the experience please let us know.

Bathroom; the attached bath has a large vanity, mirror, sink, toilet and a full size bathtub with shower, this bathroom does not have hot water. For a Hot Shower access the shared 3 piece bath just outside your room.

[**Double Bed Coconut Fiber Tatami Mats - Shared Bath**](https://www.airbnb.com/rooms/15553849?s=51)

Sleep on Nature!
Coconut tatami mats offer spinal support and are naturally breathable. Coconut fiber beds are recommended for those that suffer allergies. Enjoy a cooler more supported nights sleep. Coconut beds; sustainable and healthy. 

This room features two tatami mats each on there own custom made polynesian style wood platform. 

Bathroom; the shared bathroom has a sink, toilet and provides a Hot Shower, access the shared 3 piece bathroom located just outside your room. 

[**Single Bed Coconut Fiber Tatami Mat - Shared Bath**](https://www.airbnb.com/rooms/16730298?s=51)

Sleep on Nature!
Coconut tatami mat offer spinal support and are naturally breathable. Coconut fiber beds are recommended for those that suffer allergies. Enjoy a cooler more supported nights sleep. Coconut bed; sustainable and healthy. 

This room features one tatami mat on its own custom made polynesian style wood platform. 

Bathroom; the shared bathroom has a sink, toilet and provides a Hot Shower, access the shared 3 piece bathroom located just outside your room.
