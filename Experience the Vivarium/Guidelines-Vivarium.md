---
title: Vivarium Guidelines
date: 2018-08-20
---

We are a shoe free environment, please leave outdoor shoes at the entrance to the vivarium.

Please shower before you lay in the bed, and please do not lay in the bed with street clothes on. 

Leave windows open as much as possible for nice air flow. Open the door of your room to enjoy the flow.

We ask that guest honor our lifestyle by participating during their stay and refrain from using personal care and body products that contain chemicals, artificial scents and other harmful agents.

**Guest access**

Guests have access to the spacious living/dinning rooms, that you share with us and other guests. The kitchen is not open to guest use.

**Interaction with guests**

We are around to help with anything you need, love good conversation and respect guest privacy, engage us if you like.

**Other things to note**

Meals can be arranged.
Drinking water is available for purchase in house.
Laundry service is available for a fee.

This space is shared with ourselves, we are quite strict about our house rules. This helps provide a clean and quiet environment to stay. If you have any questions please ask prior to making your reservation.