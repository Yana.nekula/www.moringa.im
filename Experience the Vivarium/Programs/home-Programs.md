---
title: The Vivarium Eco-Immersive Experience Options
date: 2018-12-28
tags: eco, zero, waste, sustainability, urban, nomadic, lifestyles
---

# An Archaic Revival of the Symbiotic Relationship We Have With Nature.

## ***Bespoke Lifestyle ~ Luxuriate In Nature***

### **Align** *with the power that manifests everything in the universe*, **Nature.**

**Shift from a Lifestyle that was designed to; consume and betray life, back too a more Natural Symbiotic Relationship, In Harmony With Nature as the ever evolving unique Creators and Curators of Life.**

### **Evolve From The *Opulence* Provided By  Nature.**

The ecology of the natural environment is increasingly in conflict with the needs and demands of human ecology.

The nomadic way of life always tried to (and had to) maintain an equilibrium between the resources of the natural environment and the needs of the people.

## *When You Honour The Soil You Honour The Soul.*

**Personal Care and Wellbeing in Harmony Nature**, redefine what **Aging looks and FEELS like!**

***The physical body is a reflection of the Lifestyle.*** 

**Lifestyle is;** ***The reflection of your awareness of 'The Natural Abilities Your Body Has To Be Vibrant and Encourage Vitality.'***

**LUXURY**: Luxury is not a "fiat currency", the Aston Martin Valkryie in ones four car garage, ..., or other incalculable "non-life giving" indulgences. **LUXURY** is **"a state** of great **comfort** or **elegance**, a condition of **abundance** or great ease". One could arguably say that at the other end of Luxury would have to be Intelligence! 

**Habitat** is the **natural environment** of an organism, the type of place in which it is **Natural for One to Live and Grow**. Or Better Said: "a state of great **comfort** or **elegance**, a condition of **abundance** or great ease."

# Lifestyle; the Legacy ones *Karma* leaves Behind.

**Legacy; something that is a part of your history or that remains from an earlier time.** Your Habitat is part of your History.

**Karma**; is the sum of a *person's actions* in this and previous states of existence, viewed as deciding their fate in future existences. (The actions one takes today, with the actions one took yesterday, shape the future).

## The Sum Of A Persons Actions / What gets left Behind? - The totality of your Carbon and Ecological Footprint.

**Carbon/Ecological Footprint**; the amount of carbon dioxide released into the atmosphere as a result of the activities of a particular individual. The **Ecological Footprint** is defined as the biologically productive area needed to provide for everything people use: fruits and vegetables, fish, wood, fibers, absorption of carbon dioxide from fossil fuel use, and space for buildings and roads. 

**All will discover that; 'The Human Carbon/Ecological Footprint', is what the generations to come will inherit. And its the choice of the Individual what that will reflect, aesthetically and otherwise. If there is no Personal Transformation there is no Generational Transformation.** 

### With a Nomadic approach One gets close to Nature and Curates a Lifestyle that will; *'Leave a Legacy That Grows'*.

## *Bringing cultural models that worked in the past and projecting them into a Future Possibility. Living The Creative Process And Designing A Lifestyle That Feeds The Soul And Honors The Soil.* 

# Lifestyles for Change

**Live in a space of inspiration and you will be inspired.**

When one looks at a spot not far, and then one gets up and physically goes there what one has done is coordinated a short trip into the future ... our mind has said "I'm not in that place_I'm in this place, I want to be in that place" and then one goes through the steps to get there.

If there is no personal transformation there is no generational transformation. It's time to step out from the story of our past and write a New Future, a new story about healing both the Soul and the Soil.

**May the work shared here help to inspire unique journeys and bring more beauty to the individuals of the world.**

**The principles** of this system are naturaly, scale-able, duplicable, and adaptable to both Urban and Rural dwellers.

**The system** not only benefits our personal health and wellbeing but; - *Passively benefits the environment, directly impacting 4 of the top 10 Environmental Realities we face today*.

**Addressing:**

- Climate Change
- Water Pollution
- Waste Disposal
- Land Management & Urban Sprawl
- and so much more...

**Covering:**

- Gardening design and plant selection (land management)
- Eco-enzyme (water pollution)
- Bokashi composting (waste management)
- Soil rehabilitation (urban sprawl)
- Medicinal food from milk fermentation (Kefir milk)
- Laco fermenting
  
**This system offers a radical approach to:**

- ***personal care and wellbeing***
- food production
- urban renewal
- water use
- energy use
- pollution
- waste disposal
- climate Change
- and more ...

**Permaculture and Urban Renewal:** integrates **ecology**; (the branch of biology which studies the interactions among organisms and their environment), landscape, organic gardening, architecture and **agroforestry**; (bringing trees into the city and agriculture system), in creating a rich and sustainable way of living.

**Nomadism** is a livelihood form that is ecologically adjusted at a particular level to the utilization of marginal resources, thus makes use of resources that otherwise would be neglected.

***Revive The Symbiotic Relationship With Nature***

**It's Time for each of us to start comparing consumption and lifestyles, and checking this against nature's ability to provide for this consumption. It's Time to *own up to the realities* of what that translates to for the generations to come. It's time to be Ecologically Realistic and get back to the heart of what Life is all about.** 

**Having accepted this circumstance, responsibility then means the ability to have a creative response to the situation as it is Now. The *seeds of opportunity*, and awareness to take the moment and transform it into a better situation.** 

## *The Goal: For 'The Vivarium Lifestyle / Concept' to become symbiotic; Financially And Environmentally  Sustainable / Duplicable.* 

***A Shared "Living" Co-livng And Cocurate Space That Honors Privacy.*** 


## ***An Opportunity For The Creation Of Something New And Beautiful.***

**Knowledge to design a more desirable, fulfilling, life supporting; Ecologically Modern and Realistic Lifestyle.**

# ~The Vivarium Eco-Immersive Experience Options~
(in house and distance counsel)

## Stay - Experience - Learn - Share

# Resident Service Program: *Eco-Immersive ~ Vivarium Co-Host Experience* 

**Limited Avalability, Plan in advance.** 
## Live in The Vivarium and learn how to keep it alive, accept guests and share the knowledge gained.

Experience ~ Support our work and get behind-the-scenes experience with our month-long immersion program, including:

- Comfortable private accommodation
- Use of bicycles at your risk
- Full working knowledge of The Vivarium and all its programs
- Full access to kitchen and laundry; Oppertunity for energy exchanges between co-hosts and guests for services.
- Earn 10% commission on all sales made from our Fair Trade Organic Textiles that decorate the space.
- 15 hours/week of your active service, housekeeping, garden, and co-hosting. Making sure the space is clean and ready to accept guests.

Singles or couples only, one month minimum commitment.

## Learn ~ The Vivarium Fermentation Programs Included In Your Stay.  

***Kitchen:*** 

- Kefir Milk
- Kefir Cheese
- Lacto Fermentation 

***Bath & Garden:*** 

- Enzyme Body Wash 
- Bokashi composting
  
## **Full Immersive Experience, with one on one instruction and support.**  

You will be fully instructed and supported in all the **Bespoke Lifestyle Experiences** offered and able to share the knowledge with others. You will leave with a full understading of how The Vivarium functions all its systems and projects for the future. Please know we are open to colaboration and encourage individuals to share any knowledge they feel would be of benifit to this work. 

This most loved program has a lot to offer those looking to make their lifestyles more Holistic, if your interested in minimalism, zero-waste, enviromental studies... this program may be a great fit, offering a broad range of aplications. 

## INCLUSIVE PRICES: 

Single: $240USD / 1 month (30 nights)

Double: $300USD / 1 month (30 nights)

## Available Session Dates 2019/20: 

May 01 - July 31
November 01 - January 31 2020

# *Eco-Immersive ~ Vivarium Co-Host and Cocurate Experience*

## *Offer Your Style Form The Viviarium.* 

Fully customized to your needs.
Use the space for your own retreat with assistance as needed. 
Submit a proposal. ... Lets colaborate.

## The Vivarium Ideal Candidate:

- Has an open, positive, and willing attitude
- Wishes to dedicate time to a focused period of service
- Enjoys working with others as a team and proactive on their own
- Is comfortable living in a collective situation
- Is emotionally and psychologically stable
- Has physical stamina and is capable of working several hours at a stretch
- Cheerfully takes on essential physical tasks such as housekeeping and food preparation
- Is interested in learning and personal growth