---
title: Medicinal Food Garden
date: 2018-07-01
---

# Experience The Garden

*a source of great mental and physical health and wellbeing*

**The first supermarket supposedly appeared on the North American landscape in 1946. That is not very long ago. Until then, where was all the food? The food was in homes, gardens, local fields, and forests. It was near kitchens, near tables, near bedsides. It was in the pantry, the cellar, the kitchen window, the patio, the backyard.**

**Urban Medicinal Gardens can function as a producer of:**

- high value, nutrient dense, medicinal, functional food
- medicinal teas
- nursery from plant propagation(seed, seedlings, cuttings....)
- natural air conditioning
- high value compost

**This system benefits the overall well being of the human inhabitants of the Vivarium. The garden can have remarkable impacts on health, both physically and emotionally. It's part of cultivating a lifestyle that builds resiliency for the individual and community.**

**The plants not only provide medicinal and functional food, but are the live ingredient here, each pot is a mini micro climate, together they become stronger. Absorbing more and giving more. The more green space you have the cooler it is. Create you habitats with edible and medicinal greens.**

**Urban Gardens have a direct impact on; Climate Change, by bringing more organic living life into the city. Creating Urban Medicinal Food Gardens directly addresses many of the most pressing issues of *Land Management & Urban Sprawl*.**  

**Medicinal Food Garden ~ Top 6 Plant Selection Considerations (in no particular order)**

- Low days to maturation (quick to produce)
- High yield
- Functional foods - High Medicinal and Nutrient Value (multipurpose)
- Multiple harvest and long season
- Rare and threatened
- Ease of care, growth and propagation

## **Vivarium Plant Picks**

### **Aloe Vera - The Plant of Immortality. Revered for its wound healing abilities and used daily as a shaving gel by some of the male inhabitants of the Vivarium.**

**Some of the Many Proven Aloe Vera Benefits:**

- Soothes Rashes and Skin Irritations
- Treats Burns
- Heals Cold Sores
- Moisturizes Hair and Scalp
- Treats Constipation
- Helps with Digestion
- Boosts the Immune System
- Provides Antioxidants and Reduces Inflammation
- Treats Diabetes
- And the list goes on... Aloe Vera has a rich history of use.

### **Sacha Inchi - Plukenetia volubilis or Mountain Peanut (technically a seed). Seeds provide a rich and balanced oil of omega 3:6:9 oils and provide all essential amino acids, leafs can be brewed for a rich antioxidant tea.**

**The studies are thin this early on, but sacha inchi is showing promise in many areas.**

- Cholesterol related issues
- General Well being and weight loss
- Brain Health
- Heart Health
- Diabetes
- Bone Health
- Vision
- Joint Health
- Skin and Hair

### **Cranberry Hibiscus - Hibiscus acetosella (cranberry hibiscus or African rosemallow); powerful antioxidant, rich in vitamins with a beautiful lemon flavor.**

Medicinal uses include;
An infusion of the leaves in water is used as post-fever tonic and as a treatment for anaemia; (deficiency of red cells or; haemoglobin in the blood, resulting in weariness).
The leaves are crushed and soaked in cold water and the infusion is used for washing babies and young children who have body pains.

### **Ulam Raja - Cosmos caudatus, ulam raja ("the King's salad" in Malaysia). Wild Cosmos, has been used for centuries for numerous metabolic disorders, to reduce blood pressure and bone loss.**

Ulam raja has been found to increase HDL-c and reduce LDL-c. It also has antibacterial and antifungal effects and is a strong antioxidant. C. caudatus has also been shown to reduce inflammation. Ulam raja’s ability to treat at type-2 diabetes has become increasingly evident.**

# The Miracle Tree: Moringa oleifera

Moringa is one of the most impressive herbal supplements known to man. In 2008 the National Institute of Health called moringa (moringa oleifera) the “plant of the year,” acknowledging that - **“*perhaps like no other single species, this plant has the potential to help reverse multiple major environmental problems and provide for many unmet human needs*.”**

**Prior to moringa benefits being proven in scientific studies, it was used extensively in traditional medicine practices like Ayurveda medicine and found in Ancient Egyptian Tombs and Texts going back over 4,000 years.**

**To date, over 1,300 studies, articles and reports have focused on moringa benefits and this plant’s healing abilities that are important in parts of the world that are especially susceptible to disease outbreak and nutritional deficiencies.**

Research shows that just about every part of the moringa plant can be utilized in some way, whether it’s to make a potent antioxidant tea or produce an oil that hydrates and nourishes the skin.

**Throughout the world, moringa is used for treating widespread conditions:**

- inflammation-related diseases
- cancer
- diabetes
- anemia
- arthritis and other joint pain, such as rheumatism
- allergies and asthma
- constipation, stomach pains and and diarrhea
- epilepsy
- stomach and intestinal ulcers or spasms
- chronic headaches
- heart problems, including high blood pressure
- kidney stones
- fluid retention
- thyroid disorders
- low sex drive
- bacterial, fungal, viral and parasitic infections

Moringa is known by over 100 names in different languages around the world. **This easy-to-grow tropical plant species, native to the Himalayan mountains and parts of India and Africa, comes packed with over 90 protective compounds and has gained a reputation for fighting inflammation and combating various effects of malnutrition and aging, earning the nickname “*the miracle plant*.”**

**Moringa oleifera** is a deciduous tree or shrub, fast-growing, drougt-resistant, with an average height of 12 meters at maturity.

Native to the Indian  sub-continent, and naturalized in tropical and sub-tropical areas around the world.

*Some common english names:* Drumstick tree, Horseradish tree, Miracle Tree, Mothers Best Friend.

Ancient Indian writings, dating as far back as 150 B.C., show the tree was prized for its therapeutic properties. It was commonly used to protect skin, make perfume, and purity water for drinking. It was said to be the cure for over 300 diseases. 

Women in ancient Egypt reportedly rubbed Moringa seeds on their clay water pots, and dried powder from crushed seeds has been used as a handwash for many years.

In recent years, the water-clarifying ability of Moringa seed powder was found to be due to a positively-charged protein called the Moringa Oleifera Cationic Protein (MOCP). When you crush the seeds and add them to water, this protein kills the microbial organisms and causes them to clump together and settle to the bottom of the container. The cationic protein isolated from Moringa seeds kills water-borne bacteria by causing their cell membranes to fuse.

Moringa is categorised as a leafy vegetable like the leaves of the baobab, manioc, sweet potato, amaranth and hibiscus. These local leafy vegetables, either cultivated or collected, are all highly concentrated in nutrients. Their use had for a long time been shadowed by European vegetables considered more modern, such as cabbage, carrots ... etc.

**Moringa leaf** is a nutritionally rich, ecological, economical vegetable tree that grows multi-vitamins. 

- Carotene Vitamin A
- Thiamin Vitamin B1
- Riboflavin Vitamin B2
- Niacin Vitamin B3
- Vitamin C
- Calcium
- Copper
- Iron
- Magnesium
- Phosperous
- Potassium
- Protein
- Zinc

Nutritional analyses show that the leaves are very high in protein and contain all of the essential amino acids, including two amino acids that are especially important for children’s diets. This is most *uncommon* in a plant food. 

- Arginine
- Histidine
- Isoleucine
- Leucine
- Lysine
- Methionine
- Phenylainine
- Tryptophan
- Valine

Moringa leaf is a power house of nutrition. People in the Indian sub-continent have long used Moringa pods for food. Moringa leaf is a common supliment for nursing mothers as it stimulates milk production. The edible leaves are eaten throughout West Africa and in parts of Asia. Some experts claim that the Moringa leaf is our best chance at combating malnutrition in all critical parts of the sub-tropic and tropic regions of the world. ...


**Moringa Oil** has many therapeutic properties because of its unique nutrition. 

Moringa oleifera is the best known of the thirteen species of the genus Moringacae. Moringa was highly valued in the ancinet world. The Romans, Greeks and Egyptians extracted edible oil from the seeds and used it for perfume and skin lotion. Moringa oil is the most nutrient dense oil known to man. 

In the 19th century, plantations of Moringa in the West Indies exported the oil to Europe for perfumes and lubricants for fine machinery.

**Complete Nutritional Value of Moringa Oil**

- Antioxidant
- Anti-inflammatory
- Anti-microbial
- Anti-viral 
- Disinfectant
- Carrier
- Hepatoprotective
- Emollient
- Preservative
- Exfoliant
- Enfleurage

Moringa oil contains some vitamins, minerals in small amounts. However, the most striking aspect of this oil’ nutrition is the content of flavonoids, phenols and sterols. These are the prominent nutrients in moringa seed oil. 


**Vitamin E**

Moringa oil contains three varieties of Vitamin E. 
Reported- α-tocopherol (105 mg/kg), Υ-tocopherol ( 39 mg/kg) and δ-tocopherol (77 mg/kg). 

Vitamin E tocopherols are strong antioxidants. It protects the skin when applied topically and also boosts the immune system.


**Sterols**

Cold pressed moringa oil contains small amounts of many phytosterol and cholesterol as well. 

These are reported to be found in high amounts.

•β – Sitosterol – Reduces cholesterol, strong antioxidant and has shown anti-diabetic activity.

•Campesterol – Strongly anti-inflammatory and reduces osteoarthritis.

•Stigmasterol – Reduces absorption of cholesterol and lowers blood glucose.


**Flavonoids**

Even small amounts of these antioxidant compounds exert a protective effect on the body. Flavonoids strengthen the blood vessels and lower inflammatory problems. 
Total flavonoid content in moringa oil has been recorded, 18 mg RAE per gm.


**Other Benefits for the skin**

• Pacifies dry skin - Helpful in dry, irritated skin conditions like eczema and psoriasis. 

• Acne – Because of its anti-inflammatory properties,can be used as a spot treatment for acne. It also aids the body in healing acne scars.

• It adds gloss to the skin, which may be needed sometimes, especially when the weather is dry.

• It can be applied over scars left behind by wounds, scrapes, bruises and burns, powerful scar diminishing qualities.

• Fungal infections – can deal with certain fungal infections because it contains anti-fungal activity. One can use it on ringworm, athlete’s foot and jock itch.

• Anti-Aging – Regular application of oil reduces the striking appearance of wrinkles and fine lines on the skin It can be combined with an astringent product like aloe vera or witch hazel to make saggy skin taut.

• Anti-Viral – effectivly used to treat viral infections, very effective for treating cold soars. Wonderful relief from ear infection.

The seeds can be used as a flocculent to clarify water and as a source of a non-drying and very stable oil, known as Ben oil. This oil, which was once used for lubricating watches and other delicate machinery, is clear, sweet and odourless, almost never going rancid. It is edible and it is becoming increasingly popular in the cosmetics industry.

Moringa oil can be consumed oraly as a supliment, in foods, ... . The only places to avoid using moringa oil would be the eyes and you would not want to use this as a personal lubricant.

This seed oil is a powerful hair conditioner. It can be used as a hot oil conditioner to deal with nearly any hair/scalp related problem. The hot oil treatment leaves the hair well moisturized, the hair roots are nourished, dandruff is washed out and there is much less irritation on the scalp. The best result is the conditioning. Hair is manageable and can be combed nicely. It adds shine or gloss to the hair, making them look beautiful.This effect is because of "ben oil" behenic acid. Moringa oil also strengthens hair roots and as such can help with hair loss. It nourishes weak, damaged hair and reduces the lifelessness in them.

Seed oil has been identified to improve liver health in people whose liver has received damage due to toxicity. The liver, when damaged releases certain signals regarding the damage. This is monitored using serum ALT and AST levels. Studies shows that internal consumption of this oil lowered ALT and AST levels in liver damaged by a toxin. As a result, oil can be used to lower the markers of liver damage in people whoseliver is damaged by some toxins, viruses like hepatitis B, or certain medication.

Moringa oil is a nice rheumatic oil. It is applied to painful, arthritic joints. It can be use directly, or used as an oil pack, just like castor oil packs. It is effective at reducing swelling and inflammation, which provides relief from the pain in the joints. This can also be used in gout.

Seed oil can be used to calm down hysteria and uncontrolled emotional instability. This is a traditional use for the seed oil.


ALL PARTS of Moringa oleifera are medicinal.

Some common medicinal uses of Moringa oleifera:

Root:
Antilithic, rubefacient, vesicant, carminative, antifertility, anti-inflammatory, stimulant in paralytic afflictions; act as a cardiac/circulatory tonic, used as a laxative, abortifacient, treating rheumatism, inflammations, articular pains, lower back or kidney pain and constipation.

Leaf:
Purgative, applied as poultice to sores, rubbed on the temples for headaches, used for piles, fevers, sore throat, bronchitis, eye and ear infections, scurvy and catarrh; leaf juice is believed to control glucose levels, applied to reduce glandular swelling.

Stem bark:
Rubefacient, vesicant and used to cure eye diseases and for the treatment of delirious patients, prevent enlargement of the spleen and formation of tuberculous glands of the neck, to destroy tumors and to heal ulcers. The juice from the root bark is put into ears to relieve earaches and also placed in a tooth cavity as a pain killer, and has anti-tubercular activity.

Gum: 
Used for dental caries, and is astringent and rubefacient; Gum, mixed with sesame oil, is used to relieve headaches, fevers, intestinal complaints, dysentery, asthma and sometimes used as an abortifacient, and to treat syphilis and rheumatism.

Flower:
High medicinal value as a stimulant, aphrodisiac, abortifacient, cholagogue; used to cure inflammations, muscle diseases, hysteria, tumors, and enlargement of the spleen; lower the serum cholesterol, phospholipid, triglyceride, VLDL, LDL cholesterol to phospholipid ratio and atherogenic index; decrease lipid profile of liver, heart and aorta in hypercholesterolaemic rabbits and increased the excretion of faecal cholesterol.


