---
title: Bokashi Composting
date: 2018-08-13
---

# Experience The Soil Rehabilitation Magic of Bokashi

***A Journey Of Soil Rehabilitation.***

*A Revival Of The Gaian Connection; When You Heal The Soil, You Heal The Soul.*

**No garden is possible without rich soil.**

**What is Bokashi?**

Bokashi is a Japanese term meaning fermented organic matter and refers to a 2-step method of indoor composting that uses beneficial micro-organisms to quickly ferment many types of food waste including fruit, vegetables, meat, dairy and bread.

**Step 1: Fermentation** - takes place indoors in your Bokashi bucket.
As you layer your food waste with a starter mix, the microbes present in the starter mix quickly ferment the waste. Once you have filled a bucket you set it aside for 10 - 14 days and start using another bucket. Two weeks later the initial bucket of waste will be fully fermented and ready for Step 2.

**Step 2: Decomposition** - is done in the garden, compost pile, or even in your own soil factory.
Most waste will be indistinguishable from the soil in a week or two, although certain items like bones, egg shells, and corn cobs will take longer to disappear and will act as a slow release fertilizer. While the waste is decomposing it is still quite acidic so wait 10 days before planting.

The active ingredients in the process are the effective microorganisms – a combination of lactic acid bacteria (also found in kefir whey), photosynthetic bacteria, and yeast - that in effect ‘pickle’ your food waste. These microbes are present in your Bokashi Starter Mix (fermented wheat bran), which is sprinkled over the layers of food waste as you fill your bucket.

A healthy Bokashi bucket does not create bad odours, greenhouse gas, or heat - uses no power, and is completely natural. What
you should smell when you open the bucket is a slightly sweet, fermented (sour), perhaps slightly alcoholic odor and it should not be offensive.

**Benefits of Bokashi**

Bokashi is a great option for food waste for those living in apartments/condos or for those looking to use their food waste as a
nutrient rich soil amendment in their garden or plant pots.

**Bokashi can be:**

- fairly easy to maintain
- very affordable
- can use most food scraps (including meat, dairy and cooked foods) and
- provides a nutrient rich soil amendment for your garden or plant pots

**Materials Needed**

- A minimum of two 5-gallon buckets with tight fitting lids
- Absorbent material (peat moss, shredded newspaper, sawdust, dried leaf or grass...)
- Bokashi starter mix
- Food scraps chopped into small pieces

**Making Bokashi Bran / Starter Mix**

Molasses : EM/Serum : Warm water(pure) : Rice husk, sawdust (dry leaf...)

EM means effective micro-organisms. EM consists of mixed cultures of
beneficial, naturally occurring micro-organisms such as lactic acid
bacteria, yeast, photosynthetic bacteria and actinomycetes.

"EM" can be purchased online or you can make it yourself.

50ml molasses : 50ml EM/Serum : 250-300ml water : 5kg rice husk

Combine all liquids starting with 250ml of warm water.

Pour over your rice husk or sawdust, so that when you squeeze it together some of it holds but no liquid comes out. Add more water if you need.

Seal in a bag or use a bucket and put a bag over it and then a lid. You want no air or as little as possible. Seal and ferment for 10 - 14 days.

Sniff test, your bokashi should smell a little like apple cider vinegar. Its ready to use! For long term storage, lay the fermented sawdust out on shallow trays and dry in the shade. once dry you can store it in containers.

**Now you have "Bokaski Bran"**

**Making Your Own Serum**

1) Rice Water

To Make Rice Water:

1:2 rice to water ratio, soak the rice for about 15 minutes, stir vigorously then strain and ferment the water in a open air vessel with a cloth cover to allow exposure to the air for 5 - 8 days. Strain

2) Kefir whey

If you don"t have kefir Whey (made from straining kefir milk through cotton cloth to separate the milk solids from the liquid (whey), then you can add your 1 part fermented rice wash to 10 parts whole milk, cover and allow to ferment 10 - 14 days. The ferment will separate into three distinct layers. Use a turkey baster or some such device to retrieve the middle layer.
Strain.

For the serum combine:

1 part rice water : 10 parts Kefir Whey

Bottle, cap and store in a cool dark place.

Now you have your bokaski serum (for long storage you can add a little molasses)

**Inside the Bokashi Bin**

Assemble your buckets and start adding your kitchen waste.

Once you have about 1/2 inch of compacted kitchen waste dust with the bosakhi starter.

Repeat until the bin is full.

Once full seal and set aside in a warm dark corner to ferment for 2 weeks.

Bury in the garden, add to your compost bin or use your pots (up to 1/3 by volume).

**Bokashi Tea**

The liquid or Bokashi Juice that collects during the process must be removed on a regular basis. Bokashi juice contains nutrients from the food waste and is alive with micro-organisms so it makes a terrific, free fertiliser!

It is very strong so must be diluted with water at a 100:1 ratio, that's 100 parts water to 1 part bokashi juice, approximately 2 teaspoons of juice for every litre of water.

Pour the concentrated Bokashi juice or Bokashi Tea directly into kitchen and bathroom drains, toilets and septic systems. It will help prevent algae build-up and control odor. And as a huge bonus, it contributes to cleaning up our waterways as the good bacteria compete with the bad bacteria! You can also pour this liquid directly into your compost bin.