---
title: Kefir Milk
date: 2018-08-13
---

# Experience Medicinal Nutrition with Kefir Milk

Kefir is a unique cultured dairy product that’s one of the most probiotic-rich foods on the planet, and kefir benefits are incredible for healing issues like leaky gut. Kefir is a fermented milk product (cow, goat or sheep milk) that tastes like a drinkable yogurt.

**Its unique name comes from the Turkish word “keif,” which means “good feeling.”**

For centuries, it’s been used in European and Asian folk medicine due to the wide variety of conditions it’s been known to cure. It’s one of the favorite foods of the Vivarium, and pairs wonderfully with raw greens and fruits. Along with the moringa leaf, it's the foundation of our medicinal food system here in the Vivarium.

You can give your alimentary canal, or the main digestive passageway in the body, an easy boost with kefir. This nutrient- and probiotic-packed drink holds the key to helping improve many immune and digestive linked health issues. Tabbed as an “it” health food of the 21st century, kefir is a probiotic food that contains many bio-active compounds, including as many as 30 strains of good bacteria that help fight against tumors, bacteria, carcinogens and more.

Kefir is made using starter “grains,” which in reality are a combination of bacteria and yeasts that interact with the milk to make the lightly fermented drink that even lactose intolerant people can drink! It can be made from any source of milk, such as goat, sheep, cow, soy, rice or coconut. It can even be made using coconut water. Scientifically speaking, kefir grains contain a complex microbial symbiotic mixture of lactic acid bacteria and yeasts in a polysaccharide–protein matrix.

**Top 7 Kefir Benefits**

**There are many benefits to probiotic foods, and kefir is no exception. Kefir benefits are broad, and can impact the state of your daily life and health dramatically. The following are some of the top kefir benefits around.**

**1) Boosts Immunity**

Kefir contains many compounds and nutrients, that help kick your immune system into gear and protect your cells. It has a large amounts of probiotics, the special forces of the microbial world. One in particular that’s specific to kefir alone is called Lactobacillus Kefir, and it helps defend against harmful bacteria like salmonella and E. Coli. This bacterial strain, along with the various others handfuls, helps modulate the immune system and inhibit many predatory bacteria growth.

Kefir also contains another powerful compound found only in this probiotic drink, an insoluble polysaccharide called kefiran that’s been shown to be antimicrobial and help to fight against candida. Kefiran has also shown the ability to lower cholesterol and blood pressure.

**2) Builds Bone Strength**

Osteoporosis is a major concern for many people today. The deteriorating bone disease flourishes in systems that don’t get enough calcium, which is essential for bone health. Kefir made from whole fat dairy has high levels of calcium from the milk.

Perhaps more importantly it holds bioactive compounds that help absorb calcium into the body and stop bone degeneration. Kefir also contains vitamin K2, which has been shown to be vital in improving bone health, density and calcium absorption, while vitamin K deficiency can lead to bone issues. The probiotics in kefir improve nutrient absorption, and the dairy itself contains all of the most important nutrients for improving bone density, including phosphorus, calcium, magnesium, vitamin D and vitamin K2.

**3) Potentially Fights Cancer**

Kefir can be a seriously effective weapon against the spread of these multiplying and dangerous cells. The compounds found in the probiotic drink have actually shown to make cancer cells in the stomach self-destruct.

Kefir benefits in the fight against cancer are due to its large anti-carcinogenic role inside the body. It can slow the growth of early tumors and their enzymatic conversions from non-carcinogenic to carcinogenic. One in-vitro test conducted by the School of Dietetics and Human Nutrition at the Macdonald Campus of McGill University in Canada showed that kefir reduced breast cancer cells by 56 percent (as opposed to yogurt strains that reduced cells by 14 percent) in animal studies.

**4) Supports Digestion and Combats IBS**

When it comes to bacteria in the gut, it’s a tricky balance. Kefir milk and kefir yogurt help restore that balance and fight against gastrointestinal diseases like irritable bowel syndrome, Crohn’s and ulcers. Drinking kefir, loaded with probiotics, also helps your gut after taking antibiotics. The probiotic compounds help restore the lost flora that fight against pathogens. The probiotics also aid against disruptive diarrhea and other gastrointestinal side effects caused by these types of medications.

**5) Improves Allergies**

Various forms of allergies and asthma are all linked to inflammatory issues on the body.

The live microorganisms present in kefir help promote the immune system to naturally suppress allergic reactions and aid in changing the body’s response to the systemic outbreak points for allergies. Some scientists believe these allergic reactions are the result of a lack of good bacteria in the gut. Researchers from the Vanderbilt University Medical Center performed 23 different studies with almost 2,000 people, and in 17 of those studies, test subjects taking probiotics showed improved allergic symptoms and quality of life.

**6) Heals Skin**

When your gut is out of whack, it can send signals to your skin that disrupt its natural balance and cause all sorts of problems like acne, psoriasis, rashes and eczema. Kefir helps bring good bacteria back to the forefront and level out the homeostasis (stable equilibrium) for your largest organ, the skin. Kefir also benefits the skin as burn and rash treatment.

The carbohydrate found in kefir known as kefiran, aside from aiding in the immune system, has also been tested and shown helping improve the quality of skin wound healing. It’s even been shown to be protective for connective tissue.

**7) Improves Lactose Intolerance Symptoms**

The good bacteria found in many dairy products is essential for a healthy gut and body. However, there are many out there who cannot tolerate dairy because they have an adverse reaction to digesting lactose. The active ingredient in kefir helps break lactose down into lactic acid, making it easier to digest. Kefir has a larger range of bacterial strains and nutrients, some only specific to kefir, that help remove almost all of the lactose in the dairy.

Research published in the Journal of the Academy of Nutrition and Dietetics even showed that “kefir improves lactose digestion and tolerance in adults with lactose malabsorption.”

**As a disclaimer; although many people do very well with milk kefir, a small percent of people may still have issues with dairy.**

**Kefir Nutrition Facts**

*Kefir contains high levels of vitamin B12, calcium, magnesium, vitamin K2, biotin, folate, enzymes and probiotics.

Because kefir does not have a standardized nutrition content, the content values can vary based on the cows, cultures and region where it’s produced. Yet even with the range in values, kefir has superior nutrition.

For example, one cup of store-bought whole milk kefir has about:

- 160 calories
- 12 grams carbohydrates
- 10 grams protein
- 8 grams fat
- 300 milligrams calcium (30 percent DV)
- 100 IU vitamin D (25 percent DV)
- 500 IU vitamin A (10 percent DV)

**In addition, kefir contains plenty of probiotics, which is where many of the kefir benefits come from. Kefir is one of the highest probiotic foods you can eat with several important probiotic strains, and homemade kefir far outranks any store-bought variety.**

For those interested...
**Beneficial bacteria and yeasts may include the following:**
Kluyveromyces marxianus/Candida kefyr, Lactococcus lactis subsp. lactis, Lactococcus lactis subsp. cremoris, Streptococcus thermophilus, Lactobacillus delbrueckii subsp. bulgaricus, Lactobacillus casei, Kazachstania unispora, Lactobacillus acidophilus, Bifidobacterium lactis, Leuconostoc mesenteroides subsp. and Saccaromyces unisporus.

**Kefir is easy to prepare and it may be cultured to your liking. Fermenting for a shorter period produces a less sour Kefir. Longer fermentation produces more sourness, including using larger amounts of Kefir Grains.**

Experiment!

# ***Recipe for Traditional Milk Kefir***

**Ingredients & Utensils for 2 cups of Milk Kefir**

- 1 - 2 tablespoons Milk Kefir Grains
- 3 - 4 cup Clean glass jar with lid
- Nylon or stainless steel mesh strainer and spoon (wood or plastic)
- Wide bowl to strain Kefir
- 2 cups Fresh Milk (Suitable milk types: Raw, unpasteurized or pasteurized whole milk. Goat, Cow, Sheep, Buffalo, Camel and Mare’s milk...)

# *Making Milk Kefir*

**1. Place fresh Kefir Grains in clean glass jar.**

**2. Add fresh milk.**

Gently stir contents, cover with a secure lid, and let **sit at room temperature for about 24 hours, or until product thickens or sours to your liking.**

**3. Straining Liquid-Kefir**

Once Kefir has fermented to your liking, it's time to separate the Kefir Grains from the Liquid-Kefir. Placing a strainer over the open mouth of a wide container, and then pour the whole contents through the strainer.

Store the Liquid-Kefir in a clean airtight sealed bottle. Kefir may be consumed at once.

1 to 2 days storage in the fridge or ripened at room temperature improves flavor and increases nutritional value. Vitamins B6, B3 and B9(folic acid) increase during storage.

**Kefir milk if stored properly, remains safe to consume for over a YEAR.**

**4. Wash the fermenting jar and repeat.**

After straining, the grains are placed back into a clean fermenting vessel with fresh milk,
repeating the process without rinsing the grains.

*A Note on Strainers:*
Use plastic, bamboo, cane, or high quality stainless steel strainers for straining Kefir, so long
as the holes in the strainer are no larger than 2mm in diameter. Otherwise you risk losing
your grains as they will pass through and in no time you’ll have lost all your grains.

*Other Tips:*

Designate a spot away from direct sunlight for Kefir fermentation. Do not fill the fermenting
jar more than 3/4, otherwise the milk can overflow after some hours of fermentation. If the
jar is sealed tight, a slightly carbonated Kefir results. Kefir is mostly prepared allowing the
gas to escape.

Airlock is ideal but not necessary.

**Fermentation: the Key to a lush, happy, life supporting habitat for all.**

The processes required for fermentation were present on earth when man appeared on the scene. When we study fermented foods, we are in fact studying the most intimate relationships between man, microbe and food.

# Resources
**Links**

[About Kefir Grains and Kefir; their history, use, extended use, and health benefits](http://users.sa.chariot.net.au/~dna/kefirpage.html)

