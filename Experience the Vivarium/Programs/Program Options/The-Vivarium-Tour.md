---
title: Experience The Vivarium ~ Tour / Workshop
date: 2018-06-30
---

# Design A Lifestyle For Change
3 hour tour with light snack

**Nothing Beats Experience**
 
We shall come to honor all of life sooner or later. Our choices are; when that shall happen, and the quality of experience that we shall have as we learn.

A new Eco Conscious breed of Luxury Lifestyle Designers has evolved from the adage of "The American Dream", the wild places of this world need us to do better than that.

The Vivarium: An interactive space for people to explore, learn, reflect; on how we interact, impact and live in our personal environments; homes, work, travel, ..., spaces. We offer an open alternative and love collaboration.

Lifestyle; the Legacy, your Karma left Behind

**Design Lifestyles That Grow**

What we’ll do

*Explore alternative ways to heal and support the body, that heal and support your environment. Self-care that goes far beyond just the self.*

Archaic Revival- a search for Cultural Models That Worked in the past.

Knowledge to design a more desirable, fulfilling, life supporting; Ecologically Modern and Realistic Lifestyle. 

A Lifestyle that benefits the environment, directly impacting 4 of the top 10 Environmental Realities we face today. Addressing Climate Change, Water Pollution, Waste Disposal and, Land Management & Urban Sprawl.

Include more nutrition in your life. Feed the Body and Soul; Explore Urbiculture from our Medicinal Food Garden. 

Kefir Milk, a unique cultured dairy product that’s one of the most probiotic-rich foods on the planet, kefir benefits are numerous. 

A Practical solution for the Kitchen Compost; producing nutrient dense, bio-active living soil. Bokashi, a process that doesn't produce heat or CO2, the types of microbes that produce methane Can Not Survive in the acidic conditions of the bokashi bucket.

Eco-enzyme, ferment citrus peels into a nutrient dense, anti-bacterial,... life fortifying multipurpose healing wash. 

**Lifestyles for Climate Change.**

This experience includes a light lunch.

Menu:
Garden herbal tea
Drinking water

Homemade milk kefir salad
Served with garden herbs, moringa greens and fresh coconut.

Where we’ll be

The Vivarium ~ Place of Life; is located in a quiet treed residential area. The north-facing third floor unit in a low-density freehold apartment offers; great cross breeze with many large opening windows, planters and a large patio. Our private Medicinal Food Garden with over 27- 5 gallon containers, as well as 22+ clay pots on the balcony make it easy to enjoy a eco-responsible lifestyle.

Notes

Be prepared for a garden walk with appropriate clothing, and able to climb three flights of stairs.