---
title: Eco-Enzyme
date: 2018-08-13
---

# Experience Eco-Enzyme

**Eco-Enzyme - Fermented Fruit Peel (Orange and Pineapple)**


**Experience how to care for your body, home and earth, starting from your kitchen. Transform citrus peels and other naturally aromatic fruit and plant scraps into multi purpose luxury body and home care products. These enzymes are naturally; Anti-Bacterial, Anti-Fungal, Anti-Viral and a Natural Antiseptic.**

**What is Eco-Enzyme**

Eco Enzyme is an organic compound that is naturally synthesized with plant protein, minerals and juvenile hormone.

Through fermentation, the ingredients used for making Eco Enzyme will have effect on each other, creating a complex but stable enzymatic ecosystem.

**Teaming with antioxidants, Eco-Enzyme is a powerful multi-functional tool.**

Through the process of fermentation a kind of detoxification and transfer of anti-nutrients into nutrients occurs, with nutrient enhancement and the creation of micro-nutrients.

**Why should we make Eco-Enzyme?**

Cleaning products made with chemicals pollute the underground water, rivers and surrounding eco-systems. These conventional cleaning products contain harmful chemicals such as phosphate, nitrates, ammonia, chlorine etc. The accumulated effect of these chemicals released from every household causes significant damage to our health and the environment. Eco-enzyme is more than a general cleaner, organic fertilizer, air purifier, insect repellent, laundry wash, and body care product. It's a solution to a very big problem. By keeping your home and body healthy and restoring vitality and strength to the environment. It is easy to DIY at negligible cost, free of harmful pollutants, effective, and multipurpose.

**Discover the miracle eco-enzyme and how it impacts:**

- Climate Change
- Water Pollution
- Waste Disposal
- Land Management & Urban Sprawl

**Some Eco-Enzyme Benefits:**

**Safe**- Eco-Enzymes are toxic free and perfect for the most sensitive skin at any age.

**Reduce Pollution**- Methane gas released from disposed garbage can trap 21 times more heat than CO2, worsening the global warming condition.

**Multiple Usage**- Natural household cleaner; air purifier; detergent; car care; fabric softener; organic fertilizer, etc.

**Purify Underground Water**- Enzyme residues flow underground and help to purify ground water that they come in contact with.

**Purify Air**- Remove odors from toxic air released from smoking, car exhaust, chemical residues of household products, etc.

**Clearing Drains**- Eco-enzyme can be used for clearing city drains after a storm.

**Any plant matter can be used in this process, we chose citrus and pineapple peel for there powerful cleaning abilities and pleasing aroma.**

**Eco Enzyme is easy to make, three ingredients are required:**

- brown sugar
- water
- food waste

**Air Purifier**

Under fermentation ozone, O3 is emitted from the fermentation vessel, and acts
as a air purifier for the home. O3 escapes and attaches to other contaminants
in the air then breaks and becomes O2. The same act of nature happens during a
lightning storm, you’ll notice the fresh air after such an event.

**Fertilizer**

Eco Enzyme can also turn ammonia into nitrate NO3, which is a fertilizer. It
can transform carbon dioxide into CO3, which can nourish plants, fish and other
living things on the planet.

It’s important not to expose your ferment to the air, so the If the fermentation can remain active even at temperatures as high as 100C. Thought the fermentation process carbon dioxide is absorbed and oxygen released.

**Stable**

If the enzyme is full of its necessary nutrients, it can survive even at pH.
value below 3.5. A safe, stable stage to shoot for is about 4.5, no higher.

**This fermentation process leaves you with a product rich in antioxidants, which decelerate the aging of cells.**

From a baby bath to washing your bicycle! Eco-Enzyme can do it all, and more.

**Functions**

**What you can do with it:**

- Cleansing the body - natural conditioner and moisturizer for the skin. Naturaly PH balanced for personal care wash.
- Wound cleaner - burns, cuts, abrasions, bed soars, diabetic wounds ...
- Cleansing the home & drainage systems
- Kill and Repel Insects
- Fertilize plants and Rehabilitate Soil
- Purify Aquatic systems
- Cleanse Pets & Animal Cages

**How To Make Your Own Eco Enzyme**

The 1:3:10 Ratio

1. Sugar: 1 part
2. Organic waste: 3 parts
3. Water: 10 parts

**Step 1:**
Fill vessel with water leaving room for expansion, and food waste (60%  of the
volume).

**Step 2:**
Put 1 part brown sugar in the container and stir.

**Step 3:**
Put up to 3 parts food waste into the container. Sometimes I add more for a more
thick brew. This is just a guideline, experiment and play. The smaller you chop
the pieces the better your ferment.

**Step 4:**
Close the container and let the mixture ferment for 3 months. In the first month
you can open and have a peek giving a stir if you like. If your not using a
airlock you must do this daily! Leave the cap a little lose or put a balloon over
to avoid an explosion! A cloth with elastic is not suitable for this type of
fermentation. Date your bin!

**Step 5:**
Harvest Day!!! In a suitable size vessel with a suitable size strainer, separate
the solids form the liquids. Your filtered enzyme can be bottled and is ready
for use following the dilution ratios provided (see attached chart). The solids
can be used as a starter for your next batch (once only), or dried and used as
a powdered fertilizer. Add them to your garden compost or turn them directly
into the soil. Even dumping them down the toilet is perfectly fine. Of course
you will want to make sure that no large pieces remain. Often it is possible to
simply mush it up into a paste with your hands. I have been tempted to use this
as a face mask, but have yet to experiment... .

**Fermentation time & environment:** 3 months minimum, from the time you stop adding food waste. Store in a dark environment away from excessive heat.

**TIPS and Details:**

**Use brown sugar**, jaggery or some similar sugar, it is full of minerals and will leave you with a more rich product.

**Separate your kitchen waste**, think aroma and decomposition. Orange, Lemon, Pineapple, Apples, … Citrus are beautifully aromatic and make powerful cleaners.

**Use Airlocks for an anaerobic environment.** Keeping your ferment clean and free of insects. This is important for a nice functional product and for safety, containers may burst if the gases are not released.

If adding the organic matter slowly over a period of time, first fill the container to halfway with water and dissolve the sugars inside. Once your ratio of organic matter is full, top up the rest of the water and close the lid, starting the 3 month ferment.

Different organics have different benefits, aromas and textures. You can separate citrus fruits like Oranges, Lemons and Lime into a dedicated bin, as well as Pineapple, which makes for a great cleaning enzyme, and other aromatics like Ginger, Lemongrass, and Pandan can be added for a beautiful whole body washing product.

The list goes on! It’s a great idea to have a catch-all bin for everything else like veggie scraps and leftovers that is just purely 'compost' for the garden and drains. Or add these to your Bokashi Compost bin.

**If you don't intend to use your eco-enzyme, simply dump it down the toilet in reasonable portions, taking care to ensure no large chunks remain. Use for the garden, or you can dump it in the sewer drain outside in the street.**

**Other Tips**

- Cut the ingredients into small pieces to let them break down faster.
- Make sure all the organic ingredients are covered by the sugar water.
- Don’t use oily food waste (fish and meat) as ingredients as it creates a bad smell.
- Leave some space at the top of the container to let the mixture ferment.
- Put your enzyme container in a well ventilated and cool space away from direct sunlight.
- If small insects are found in the container, close the lid tightly and let those insects
decompose. (Increasing protein content!)
- A white layer on the surface of Eco Enzyme is a sign that fermentation is underway.
- Don’t forget to label your ‘Start Date’ on the container!
- The best Enzyme ferments are 3 months and longer.

Remember: The 3-month period for fermentation starts once your last
organic ingredients are added into the container.

**Using your enzyme**

**See Chart**

**Favored usage in the Vivarium include:**

- Hair and body wash
- Dish washing
- Fruit and vegetable wash
- Evaporative cooler - air freshener
- Insect replant and fertilizer for the garden
- Laundry detergent ... The list goes on.

**Frequently Asked Questions**

Q: Should I use only one kind of fruit peels, or many different kinds?

A: Using various ingredients can create complex and stable enzymatic ecosystems. Therefore, it is better to use different kinds of fruit peels and vegetable leaf. However! If you’re looking for a product with more specific fragrance profiles, you might want to be more selective.


Q: Will there be bacteria in Eco-Enzyme after fermentation?
Is it safe to be used for personal cleaning?

A: Eco-Enzyme contains acetic acid, probiotics, and antioxidants, which help fight bad bacteria on the skin’s surface. Eco Enzyme can reduce allergies caused by chemicals, eliminate harmful microorganisms and enhance cell regeneration. It is important to remember that the acidic value of Eco-Enzyme sits below PH 5.0 if it has been fermented well, so remember to dilute accordingly. Eco-Enzyme is very safe and makes a wonderfully gentle yet powerful wash for wound cleaning. Safe for all family members to use.


Q: How can we make sure that the Eco-Enzyme is well fermented?

A: If the fermentation is successful, the liquid will turn yellowish brown and it will form a white layer on the surface. Otherwise, it will turn black or become moldy. If it turns black, we only have to add brown sugar (same amount) and let it ferment for another month.
